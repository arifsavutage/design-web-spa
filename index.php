<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Be fit adalah website layanan jasa terapis pijat ataupun spa online.">
  <meta name="keywords" content="jasa terapis semarang, spa online, jasa pijat spa, pijat semarang">
  <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="theme.css" type="text/css">
  <!--
  <link rel="stylesheet" href="bootstrap4/css/bootstrap.min.css" type="text/css">
-->
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="#">
        <!--<i class="fa d-inline fa-lg fa-cloud" ></i>-->
        <img src="images/logo.png">
        <b> BeFit</b>
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#paket">Paket Spa</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#artikel">Artikel</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#kontak">Kontak</a>
          </li>
        </ul>
        <a class="btn navbar-btn ml-2 btn-success text-light" href="#" onclick="window.location.href='https://api.whatsapp.com/send?phone=6281234567890&text=Hallo%20Kak,%20mau%20tanya%20paket%20spa%20dong'">
          <i class="fa d-inline fa-lg fa-whatsapp"></i> Chat</a>
      </div>
    </div>
  </nav>
  <div class="py-5 text-center" style="background-image: url('images/cover-image-001.jpg');background-repeat:no-repeat;background-size:cover;background-position:center top;">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">
          <h1 class="display-3 mb-4 text-white">Selamat datang di website
            <br> Be Fit Spa</h1>
          <p class="lead mb-5 text-white">Be Fit Spa adalah layanan penyedia jasa terapis untuk Spa &amp; Massage,
            <br>yang memiliki cakupan area Semarang - Kendal dan sekitarnya</p>
          <a href="#kontak" class="btn btn-lg mx-1 btn-warning">Contact Us</a>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1 class="text-center display-3 text-primary">Daily Quotes</h1>
          <blockquote class="text-center border"> Love yourself. It is important to stay positive because beauty comes from the inside out </blockquote>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 text-white bg-secondary" id="paket">
    <div class="container">
      <div class="row">
        <div class="align-self-center p-5 col-md-6">
          <h1 class="mb-4">Paket Spa &amp; Massage</h1>
          <p class="mb-5">Be Fit Spa &amp; Massage memiliki tenaga terapis atau pemijat pria &amp; wanita muda yang rata-rata sudah memiliki pengalaman yang cukup lama dibidang Spa &amp; Massage. Kami memiliki terapis yang ramah, sopan, wangi, bersih, dan tentunya juga
            profesional dan terpercaya </p>
          <p class="mb-5" style="background-color: red;padding:15px;font-size: 28px;color: #ffffff;">
            <strong>Warning :</strong>
            <br> Be Fit Spa &amp; Massage Tidak Melayani Pijat Plus-Plus !!</p>
          <a class="btn btn-lg btn-outline-light" href="#" onclick="window.location.href='https://api.whatsapp.com/send?phone=6281234567890&amp;text=Hallo%20Kak,%20mau%20tanya%20paket%20spa%20dong'">Pesan Sekarang</a>
        </div>
        <div class="col-md-6 p-0">
          <div id="carousel1" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img class="d-block img-fluid w-100" src="images/paket-001.jpg" atl="first slide">
                <div class="carousel-caption">
                  <h3>Traditional Body Massage</h3>
                </div>
              </div>
              <div class="carousel-item">
                <img src="images/paket-002.jpg" class="d-block img-fluid w-100" data-holder-rendered="true">
                <div class="carousel-caption">
                  <h3>Traditional Body Scrub</h3>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid w-100" src="images/paket-003.jpg" data-holder-rendered="true">
                <div class="carousel-caption">
                  <h3>Traditional Facial Massage</h3>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid w-100" src="images/paket-004.jpg" data-holder-rendered="true">
                <div class="carousel-caption">
                  <h3>Traditional Facial Scrub</h3>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid w-100" src="images/paket-005.jpg" data-holder-rendered="true">
                <div class="carousel-caption">
                  <h3>Facial Acupressure Massage</h3>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
              <span class="carousel-control-next-icon"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5" id="artikel">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-7">
          <h2 class="text-primary">Stop pekerjaan Anda sejenak, lakukan gerakan kecil ini dan rasakan manfaatnya</h2>
          <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <a class="btn btn-lg btn-outline-primary"
            href="#">Selengkapnya</a>
        </div>
        <div class="col-md-5 align-self-center">
          <img class="img-fluid d-block w-100 img-thumbnail" src="images/artikel-001.jpg"> </div>
      </div>
      <div class="row">
        <div class="col-md-5">
          <img class="img-fluid d-block mb-4 w-100 img-thumbnail" src="images/artikel-002.jpg"> </div>
        <div class="col-md-7">
          <h2 class="text-primary pt-3">5 pijatan diwajah ini ternyata dapat menyegarkan wajah Anda</h2>
          <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <a class="btn btn-lg btn-outline-primary"
            href="#">Selengkapnya</a>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" href="#">
                <span>«</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">3</a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">4</a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                <span>»</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 bg-dark text-white" id="kontak">
    <div class="container">
      <div class="row">
        <div class="col-md-6 text-center align-self-center">
          <p class="mb-5" id="kontak">
            <strong>Kontak Cutomer Service :</strong>
            <br>Nova
            <br>
            <abbr title="Phone">P:</abbr> +6281 234 567 890 </p>
          <div class="my-3 row">
            <div class="col-4">
              <a href="https://www.facebook.com" target="_blank">
                <i class="fa fa-3x fa-facebook"></i>
              </a>
            </div>
            <div class="col-4">
              <a href="https://twitter.com" target="_blank">
                <i class="fa fa-3x fa-twitter"></i>
              </a>
            </div>
            <div class="col-4">
              <a href="https://www.instagram.com" target="_blank">
                <i class="fa fa-3x fa-instagram"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-6 p-0">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d53280.446428255345!2d110.41448286379017!3d-6.998343709891914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708b4d3f0d024d%3A0x1e0432b9da5cb9f2!2sSemarang%2C+Kota+Semarang%2C+Jawa+Tengah!5e0!3m2!1sid!2sid!4v1538364413038"
            width="600" height="300" frameborder="0" style="border:0" allowfullscreen=""></iframe>
        </div>
      </div>
    </div>
  </div>
  
  <script src="bootstrap4/js/jquery-3.2.1.slim.min.js"></script>
  <script src="bootstrap4/js/popper.min.js"></script>
  <script src="bootstrap4/js/bootstrap.min.js"></script>
</body>

</html>